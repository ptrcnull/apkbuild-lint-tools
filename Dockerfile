FROM alpine:edge

RUN apk add --no-cache abuild doas atools spdx-licenses-list git \
    && adduser -D lint

COPY overlay/ /

RUN chmod 0600 /etc/doas.d/buildozer.conf

COPY --from=koalaman/shellcheck /bin/shellcheck /bin/shellcheck

USER lint

CMD changed-aports master | lint
